#include "algo_bench.h"

#include <iostream>
#include <ctime>
#include <chrono>
#include <vector>

///Измеряем время операции load
//https://stackoverflow.com/questions/22387586/measuring-execution-time-of-a-function-in-c
auto measureOperation(std::function<void(void)> load)
{
    auto t0 = std::chrono::high_resolution_clock::now();
    load();
    auto t1 = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::milliseconds>(t1-t0);
}

///Генерируем массив случайных значений
//https://en.cppreference.com/w/cpp/numeric/random/rand
//https://en.cppreference.com/w/cpp/numeric/random/uniform_int_distribution
std::vector<int> genRandJob(int size)
{
    std::vector<int> res(size);
    std::srand(std::time(nullptr));
    for(auto& elem : res)
        elem = std::rand();
    return res;
}

///Проверяем упорядоченность массива случайных значений
bool checkSortOrder(const std::vector<int>& job)
{
    for(auto i = job.size()-1; i>0; --i)
        if(job[i-1]>job[i])
            return false;
    return true;
}

/// Проводим несколько измерений времени выполнения алгоритма сортировки
/// Прекращаем после 20 точек измерения, либо после превышения времени одного прохода сортировки 30ти секунд
void benchMarkAlgo(const char* algoName, std::function<void(int*, int*)> algo)
{
    std::wcout << algoName << std::endl;
    std::wcout << "N\tt_msec" << std::endl;
    for(int i = 1, step = i, point = 0/*, last_step = step, step_rize = step*/; i<pow(2,10*10);)
    {
        auto job = genRandJob(i);
        auto algoLoad = [&job, algo](){
            algo(&job.front(),&job.back());
        };
        auto duration_msec = measureOperation(algoLoad);
        auto test = checkSortOrder(job);
        auto duration = duration_msec.count();
        if(duration>0 || !test)
            std::wcout << i << "\t" << duration << "\t" << (test ? "Ok" : "FAIL!") << std::endl;
        if(!test)
            break;

        //Считаем приращение шага для проведения очередного теста
        i+=step;
        if(duration>1000)///Больше, чем 100 мсек
            i+=step;
        else if(duration>100)///Больше, чем 100 мсек
            step*=1.44;
        else if(duration==0)
        {
            step*=2;
            continue;
        }
        if(point++>20 || duration>30000) //Если выдали больше 20 точек для графика, либо длительность одного прохода больше 30 секунд
            break; //Завершаем операцию
    }
}
