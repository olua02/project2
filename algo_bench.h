#ifndef ALGO_BENCH_H
#define ALGO_BENCH_H

#include <functional>

/// Проводим несколько измерений времени выполнения алгоритма сортировки
/// Прекращаем после 20 точек измерения, либо после превышения времени одного прохода сортировки 30ти секунд
void benchMarkAlgo(const char* algoName, std::function<void(int*, int*)> algo);

#endif // ALGO_BENCH_H
