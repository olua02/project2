﻿#include "sort_algos.h"

#include <algorithm>

///Сортировка пузырьком
void bubleSort(int* start, int* finish)
{
    bool replacement = false;
    do {
        replacement = false;
        for(int* i = start; i<finish; ++i)
            if(*i>*(i+1))
            {
                swap(*i, *(i+1));//То же, что и выше, но в одну строчку, меняет значения местами
                replacement = true;
            }
    } 
    while(replacement);
}

///Сортировка перемешиванием (шейкерная сортирвка, или двунаправленная)
void shakerSort(int* start, int* finish)
{
    int leftBord = 0;
    int rightBord = finish - start;
   
    while(leftBord <= rightBord)
    {
        for(int* i = start; i<finish; i++)
            if(*i>*(i+1))
                std::swap(*i, *(i+1));
        rightBord--;
        
        for(int* i = finish; i>start; i--)
            if(*i<*(i-1))
                std::swap(*i, *(i-1));
        leftBord++;
    }
}

void mergeSort(int *start, int *finish)
{
  int cnt = finish-start + 1;
  int step = 1;  
  int *subsidiary = (int*)malloc(cnt * sizeof(int)); 
  while (step < cnt) 
  {
    int index = 0;   
    int leftBord = 0;      
    int aim = leftBord + step; 
    int rightBord = leftBord + step * 2; 
    do
    {
      aim = aim < cnt ? aim : cnt;  
      rightBord = rightBord < cnt ? rightBord : cnt;
      int i1 = leftBord, i2 = aim;
      while(i1 < aim && i2 < rightBord ) 
        if (*(start+i1) < *(start+i2)) 
            subsidiary[index++] = *(start+i1++); 
  
        else 
            subsidiary[index++] = *(start+i2++);
 
      while (i1 < aim) {subsidiary[index++] = *(start+i1); i1++;} // заносим оставшиеся элементы сортируемых участков
      while (i2 < rightBord) {subsidiary[index++] = *(start+i2); i2++;} // в результирующий массив
      leftBord += step*2, aim += step*2, rightBord += step*2; 
    } 
    while (leftBord < cnt); 
    for (int i = 0; i < cnt; i++) 
      *(start+i) = subsidiary[i];
    step *= 2; 
  }
}

///Сортировка с помощью двоичного дерева (tree sort)
struct Node 
{ 
    struct Node *left, *right; 
    int val;
}; 
 
struct Node *createNode(int item) 
{ 
    struct Node *temp = new Node; 
    temp->val = item; 
    temp->left = temp->right = NULL; 
    return temp; 
} 
 
void storeSorted(Node *tree, int* start, int* finish, int &i) 
{
    if (tree != NULL) 
    { 
        storeSorted(tree->left, start, finish, i); 
        *(start + i++) = tree->val; 
        storeSorted(tree->right, start, finish, i); 
    } 
 
} 
 
Node* insert(Node* node, int val) 
{ 
    if (node == NULL) return createNode(val); 
 
    if (val < node->val) 
        node->left  = insert(node->left, val); 
 
    else if (val > node->val) 
        node->right = insert(node->right, val); 
 
    return node; 
} 
 
void treeSort(int* start, int* finish) 
{ 
    int n = finish-start + 1;
    struct Node *tree = NULL; 
    tree = insert(tree, *start); 
 
    for (int i=1; i<n; i++) 
        tree = insert(tree, *(start + i)); 
    int i = 0; 
    storeSorted(tree, start, finish, i); 
} 
