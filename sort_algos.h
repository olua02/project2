#ifndef SORT_ALGOS_H
#define SORT_ALGOS_H

///Сортировка пузырьком
void bubleSort(int* start, int* finish);

///Сортировка перемешиванием (шейкерная сортирвка, или двунаправленная)
void shakerSort(int* start, int* finish);

///Сортировка слиянием (merge sort)
void mergeSort(int* start, int* finish);

///Сортировка с помощью двоичного дереча (tree sort)
void treeSort(int* start, int* finish);

#endif // SORT_ALGOS_H
